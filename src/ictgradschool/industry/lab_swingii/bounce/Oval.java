package ictgradschool.industry.lab_swingii.bounce;

public class Oval extends Shape {
    public Oval() {
        super();
    }
    public Oval(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public Oval(int x, int y) {
        super(x,y);
    }

    public Oval(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }



    @Override
    public void paint(Painter painter) {painter.drawOval(fX, fY, fWidth, fHeight);

    }
}
