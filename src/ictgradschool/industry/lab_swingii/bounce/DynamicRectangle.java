package ictgradschool.industry.lab_swingii.bounce;

import java.awt.*;

public class DynamicRectangle extends Shape {
    private boolean drawEmpty;
    private int lastDeltaX;
    private int lastDeltaY;

    public DynamicRectangle() {
    }

    public DynamicRectangle(int x, int y) {
        super(x, y);
    }

    public DynamicRectangle(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public DynamicRectangle(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    int winWidth;
    int winHeight;

    public void setWinWidth(int winWidth) {
        this.winWidth = winWidth;
    }
    public void setWinHeight(int winHeight) {
        this.winHeight = winHeight;
    }
    @Override
    public void paint(Painter painter) {
//this.winWidth;

        /*if(this.fY==0||this.fY>=300){
            lastBounceTopOrBottom=true;
        }
        if(this.fX==0||this.fX>=300){
            lastBounceTopOrBottom=false;
        }
        System.out.println(this.fX+" "+winWidth+" "+ this.fY+" "+ winHeight+ " "+lastBounceTopOrBottom);
        //if it bounces off top or bottom draw outline

        if(lastBounceTopOrBottom) {
        painter.drawRect(fX,fY,fWidth,fHeight);}
        else{
        //if it bounces off left or right draw solid rectangle
        painter.fillRect(fX,fY,fWidth,fHeight);}

*/

        if (fDeltaX == -lastDeltaX){
            drawEmpty = false;
        }
        if (fDeltaY == -lastDeltaY){
            drawEmpty = true;
        }
        if(drawEmpty) {
            painter.setColor(Color.red);
            painter.fillRect(fX,fY,fWidth,fHeight);}
        else{
            //if it bounces off left or right draw solid rectangle
            painter.setColor(Color.blue);
            painter.fillRect(fX,fY,fWidth,fHeight);}

        lastDeltaX = fDeltaX;
        lastDeltaY = fDeltaY;

    }
}
