package ictgradschool.industry.lab_swingii.bounce;

import java.awt.*;

public class Gem extends Shape {

    public Gem() {
        super();
    }

    public Gem(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public Gem(int x, int y) {
        super(x, y);
    }

    public Gem(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }


    @Override
    public void paint(Painter painter) {

        int[] xpoints;
        int[] ypoints;
        if (fWidth > 40) {
            xpoints = new int[]{fX + 20, fX + fWidth - 20, fX + fWidth, fX + fWidth - 20, fX + 20, fX};
            ypoints = new int[]{fY, fY, fY + fHeight / 2, fY + fHeight, fY + fHeight, fY + fHeight / 2};
            int npoints = 6;
            painter.drawPolygon(new Polygon(xpoints, ypoints, npoints));
        } else {
            xpoints = new int[]{fX+fWidth/2,fX+ fWidth, fX+ fWidth/2,fX};
            ypoints = new int[]{fY, fY+fHeight/2, fY+fHeight, fY+fHeight/2};
            int npoints = 4;
            painter.drawPolygon(new Polygon(xpoints, ypoints, npoints));
        }
    }
}



